import React from 'react';
import Card from '../Card/Card';

const Movies = props => {
    return(
        <div className="Movies">
            <Card name={props.name1} year={props.year1} picture={props.picture1}/>
            <Card name={props.name2} year={props.year2} picture={props.picture2}/>
            <Card name={props.name3} year={props.year3} picture={props.picture3}/>
        </div>
    )
}

export default Movies;