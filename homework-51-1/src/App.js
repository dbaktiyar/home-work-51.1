import React, { Component } from 'react';
import './App.css';
import Movies from './Movies/Movies'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Movies name1="Бегущий по лезвию" year1="2017" picture1="../f18c517ef5ea922e999fcf2c1f9b20d1_200x300.jpg"
                name2="Крушение Белого дома" year2="2017" picture2="../de2de98048f55a6edbbd7ff7fd03e1d6_200x286.jpg"
                name3="Телохранитель киллера" year3="2017" picture3="../383d02e6c7890eaa38f0abdf083c60ca_200x296.jpg"  />
      </div>
    );
  }
}

export default App;
