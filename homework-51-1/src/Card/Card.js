import React from 'react'
import './Card.css'

const Card = props => (
    <div className="Card">
        <h1>{props.name}</h1>
        <p>{props.year}</p>
        <img src={props.picture} alt="" width="200px" height="auto"/>
    </div>
);

export default Card;